from django.forms import ModelForm, HiddenInput, ChoiceField
from organiser.models import Test, MachineProfile, Errata


class TestForm(ModelForm):

    number = ChoiceField(())

    def clean_result(self):  # pylint: disable=no-self-use
        return Test.RESULT_CLAIMED

    def __init__(self, *args, **kwargs):
        self.selections = []
        super(TestForm, self).__init__(*args, **kwargs)
        self.selections = self.initial["numbers"]
        self.fields["number"] = ChoiceField(self.selections)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({"class": "form-control"})

    class Meta:
        model = Test
        fields = ["number", "claimer", "language", "network", "profile", "result"]
        widgets = {
            "claimer": HiddenInput(),
            "profile": HiddenInput(),
            "result": HiddenInput(),
        }


class UpdateForm(ModelForm):
    # Need to support cancelling a claimed test.
    # Need to support creating an Errata on reporting a fail.

    class Meta:
        model = Test
        fields = [
            "number",
            "claimer",
            "profile",
            "result",
            "language",
            "image",
            "disk",
            "filesystem",
            "desktop",
        ]
        widgets = {
            "number": HiddenInput(),
            "claimer": HiddenInput(),
            "profile": HiddenInput(),
            "result": HiddenInput(),
            "language": HiddenInput(),
            "image": HiddenInput(),
            "disk": HiddenInput(),
            "filesystem": HiddenInput(),
            "desktop": HiddenInput(),
        }


class AddErrata(ModelForm):
    class Meta:
        model = Errata
        fields = ["content", "profile"]
        widgets = {"test": HiddenInput(), "profile": HiddenInput()}


class VirtualProfileForm(ModelForm):
    class Meta:
        model = MachineProfile
        fields = [
            "name",
            "architecture",
            "command_line",
            "emulator",
            "hardware",
            "owner",
            "cpu_type",
            "ram_size",
            "disc_size",
            "boot_method",
            "sound",
            "resolution",
            "screen_keybd",
            "serial",
            "ethernet",
            "wifi",
            "network",
        ]
        widgets = {"owner": HiddenInput(), "hardware": HiddenInput()}


class BareMetalProfileForm(ModelForm):
    class Meta:
        model = MachineProfile
        fields = [
            "name",
            "architecture",
            "manufacturer",
            "hardware",
            "owner",
            "model_name",
            "cpu_type",
            "ram_size",
            "disc_size",
            "boot_method",
            "sound",
            "resolution",
            "screen_keybd",
            "serial",
            "ethernet",
            "wifi",
            "network",
        ]
        widgets = {"owner": HiddenInput(), "hardware": HiddenInput()}
