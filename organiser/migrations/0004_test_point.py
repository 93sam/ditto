# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2020-02-01 13:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("organiser", "0003_auto_20200104_1826")]

    operations = [
        migrations.AddField(
            model_name="test",
            name="point",
            field=models.CharField(
                default="10.0.0",
                help_text="Debian Point Release",
                max_length=1024,
                verbose_name="Point release",
            ),
            preserve_default=False,
        )
    ]
