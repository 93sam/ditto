.. index:: Introduction

.. _introduction:

Introduction
############

.. _creating_account:

Creating a new account
**********************

From the home page, click on **Create new account** and enter your
details. When created, your user will be in **Moderated** status.
Please ask one of the admins to set your account as **Active** by
asking on the `#debian-cd` IRC channel.

.. _active_users:

Active users
************

Once your status is **Active**, you can create one or more machine 
profiles and then claim a test to run Debian Installer on that
machine.

.. _machine_profiles:

Machine profiles
****************

By default, three architectures are supported for any machine profile:

* AMD64 https://www.debian.org/ports/amd64/
* i386 https://www.debian.org/ports/i386/
* ARM64 https://wiki.debian.org/Arm64Port

If your machine is a different architecture, ask one the admins on the 
`#debian-cd` IRC channel to create this architecture in the Ditto 
database.

Each machine profile has a friendly name which you will use to select
this machine when claiming a test. Please make it descriptive to help
you identify which test ran on which machine when it comes to updating
the test results.

Common settings for all profiles
================================

RAM Size
--------

Please specify how much memory was configured for this machine. (This 
may already be included in the command line above but it is still 
useful to enter it here.)

Disc size
---------

Please specify how much disc space was allocated for this machine. 
(This may already be included in the command line above but it is still 
useful to enter it here.)

We recommend that you allow a minimum of 16GB disk space for an 
installation test, If you intend to a sparse disk format for a VM please
ensure that you can allocate at least 8GB of real disk space.  Over time 
these minimums may need to be increased - especially for the larger 
desktop environments, gnome and kde we are looking at you :-)
 
 
Notes: Some of the tests require multiple partitions.  DI does a 
reasonable job of sizing partitions based on available disk space.  
However for smaller disks (i.e. <50GB) we recommend that more space
is allocated to the '/' part ion.  As installation tests are typically 
throw away environments, swapping the space DI recommends for the
'/' and '/home' partitions is a good idea.

Boot method
-----------

Please select from the drop down menu whether you will use UEFI or BIOS to 
boot the machine. If you are unsure, ask on the `#debian-cd` IRC 
channel.

Sound
-----

If you have configured sound (enough to be able to listen to 
synthesised speech produced by Debian Installer), set the checkbox for
sound.  Sound is used for performing installation tests with a speach 
synthesiser, so it will need to be available without any special drivers
or firmware.

Serial
------

This is more common with a bare metal profile but may also be used
with virtual machines, for example if you are executing the virtual
machine on a remote host of a different architecture. 

Ethernet
--------

Please set the checkbox if the machine has an ethernet adaptor 
configured. This will only need to be able to download files from the 
internet.

WiFi
----

Please set the checkbox if Debian Installer is able to use WiFi 
directly.

.. note:: This setting is **not** to be used if the machine you are 
   using to execute a virtual machine is connected via WiFi. Only if
   the virtual machine itself has a WiFi adaptor configured.


Virtual Machine profiles
========================

Virtual Machines can be run using `libvirt 
<https://tracker.debian.org/pkg/libvirt>`_, `Virtual Machine Manager 
<https://tracker.debian.org/pkg/virt-manager>`_, `QEMU 
<https://tracker.debian.org/pkg/qemu>`_ or `VirtualBox 
<https://tracker.debian.org/pkg/virtualbox>`_.

In addition to the common settings for all machine profiles, virtual
machine profiles support:

Command line
------------

Please enter the full command line you use to run the Debian Installer 
using this virtual machine. If you used a GUI like Virtual Machine 
Manager or VirtualBox, just enter the name of the GUI.

Emulator
--------

Please specify the emulator being used, if any - typically QEMU would
be used to emulate a differant platform.


Bare Metal profiles
===================

A bare metal profile would be defined for a laptop or desktop computer
(or for some architectures, a developer kit) which is booting directly
from the Debian Installer media. This will involve creating new 
partitions on the specified storage of such a machine and this can 
cause permanent data loss on that machine. 

Whilst it is possible to test Debian Installer in a multi-boot situation
where Debian is to share the storage with another operating system, 
there are some tests which will cause any another operating system 
to be erased, hidden or disabled. It is recommended that any machine 
used for Ditto has no important data stored on the system. 

Please do not use your primary computer for testing as a bare
metal machine profile in Ditto.

We will always try and reproduce tests that have failed as a VM on a 
bare metal install.

Extra settings for a bare metal profile are:

Manufacturer
------------

Useful for debug and to distinguish one profile from another.

Model Type
----------

Useful for debug and to distinguish one profile from another.

CPU Type
--------

Useful for debug and to distinguish one profile from another.

Resolution
----------

The resolution of any monitor used with this bare metal profile, for
example when testing a desktop installation.

Screen and keyboard
-------------------

Enable the checkbox if the bare metal machine profile can use a screen
and a physical keyboard.

