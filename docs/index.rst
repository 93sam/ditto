.. DITTO documentation master file, created by
   sphinx-quickstart on Sun Jul  7 17:04:31 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. index:: ditto

Welcome to DITTO's documentation!
#################################

Debian Images Team - Test Organiser
***********************************

The test organiser seeks to provide a friendly interface to selecting
and tracking the testing operations done by the Debian Images Team
during the preparation of the download images for a Debian release,
including point releases.

The team produce images suitable for CDs and DVDs to help people 
install Debian. That means that we need to work regularly with other 
teams to make things work:

* `debian-installer <https://wiki.debian.org/DebianInstaller>`_
* `the Release Team <https://wiki.debian.org/Teams/ReleaseTeam>`_
* `the architecture porters <https://wiki.debian.org/Ports>`_

Then the work splits into two parts:

* Developing the debian-cd software itself

* Using that software regularly to generate and publish CD and DVD 
  images, both daily/weekly for testing images and straight after each 
  release for official stable images.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   claiming
   developer

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
