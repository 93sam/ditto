from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from organiser.views import (
    index,
    offer_test,
    update_test,
    amend_profile,
    test_errata,
    view_errata,
    create_account,
    create_virtual_profile,
    create_bare_profile,
    testlog,
)

# pylint: disable=invalid-name

urlpatterns = [
    url(r"^$", index, name="index"),
    url(r"^claim/(?P<pk>[0-9]+)$", offer_test, name="testoffer"),
    url(r"^amend/(?P<pk>[0-9]+)$", amend_profile, name="machineupdate"),
    url(r"^update/(?P<pk>[0-9]+)$", update_test, name="testupdate"),
    url(r"^errata/(?P<pk>[0-9]+)$", test_errata, name="testerrata"),
    url(r"^viewerrata/(?P<pk>[0-9]+)$", view_errata, name="viewerrata"),
    url(r"^createaccount/", create_account, name="createaccount"),
    url(r"^createvirtual/", create_virtual_profile, name="createvirtual"),
    url(r"^createbaremetal/", create_bare_profile, name="createbaremetal"),
    url(r"^testlog/(?P<pk>[0-9]+)$", testlog, name="testlog"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
