from django.contrib.auth.models import User
from django.test import TestCase
from django.db.models import Q
from organiser.models import Profile, Test, Architecture, MachineProfile, DILanguage
from organiser.utils import ReleaseSet, InstallerImage


# This package is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation
# .
# This package is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
# .
# You should have received a copy of the GNU Affero General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


class TestClaims(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="first", email="second@third")
        self.user.profile.nick = "fourth"
        self.user.active = True
        self.user.save()
        self.r_set = ReleaseSet()
        Architecture.objects.get_or_create(name="amd64")
        self.arch = Architecture.objects.get(name="amd64")
        self.machine = MachineProfile.objects.create(
            architecture=self.arch,
            hardware=MachineProfile.HARDWARE_VIRTUAL,
            boot_method=MachineProfile.BOOT_BIOS,
            name="tests_qemu_bios_amd64_1",
            command_line="qemu-system-x86 {{ imagefile }}",
            emulator="qemu",
            ram_size="2G",
            disc_size="10G",
            screen_keybd=True,
            owner=self.user,
        )
        self.language = DILanguage.objects.create(name="en-gb.UTF-8")

    def tearDown(self):
        Test.objects.all().delete()
        MachineProfile.objects.all().delete()
        Architecture.objects.all().delete()
        DILanguage.objects.all().delete()

    def test_start_claim(self):
        self.assertEqual([], list(Test.objects.all()))
        self.assertIsInstance(self.r_set.point_releases, set)
        self.assertNotEqual(set(), self.r_set.point_releases)
        priorities = self.r_set.find_image(self.machine)
        for pri in priorities:
            self.assertIsInstance(pri, InstallerImage)
        options = [image for image in self.r_set.find_image(self.machine)]
        prof = Profile.objects.get(user=self.user)
        testing = options[0].name
        self.assertIsInstance(options[0].data, dict)
        claimed = Test.objects.create(
            claimer=prof, image=testing, language=self.language, profile=self.machine
        )
        options[0].populate_test(claimed)
        claimed.refresh_from_db()
        self.assertIsNotNone(claimed)
        self.assertEqual(Test.RESULT_UNCLAIMED, claimed.result)
        # mimic what the form validation does
        claimed.clean()
        claimed.refresh_from_db()
        self.assertEqual(Test.RESULT_CLAIMED, claimed.result)
        options = [image for image in self.r_set.find_image(self.machine)]
        claims = Test.objects.filter(
            Q(claimer=prof, image=testing, language=self.language, profile=self.machine)
            & ~Q(result__in=[Test.RESULT_CLAIMED, Test.RESULT_PASS])
        )
        self.assertEqual(list(claims), [])
        for test in Test.objects.all():
            self.assertIsNotNone(test.number)
            self.assertIsNotNone(test.filesystem)
            self.assertIsNotNone(test.disk)
            self.assertIsNotNone(test.desktop)
            self.assertIsNotNone(test.point)
            self.assertIsNotNone(Test.INSTALL_CHOICES[test.mode])
            self.assertIsNotNone(MachineProfile.BOOT_CHOICES[test.profile.boot_method])

    def test_update_claim(self):
        self.assertEqual([], list(Test.objects.all()))
        priorities = self.r_set.find_image(self.machine)
        for pri in priorities:
            self.assertIsInstance(pri, InstallerImage)
        options = [image.name for image in self.r_set.find_image(self.machine)]
        prof = Profile.objects.get(user=self.user)
        testing = options[0]
        claimed = Test.objects.create(
            claimer=prof, image=testing, language=self.language, profile=self.machine
        )
        claimed.refresh_from_db()
        self.assertIsNotNone(claimed)
        self.assertEqual(Test.RESULT_UNCLAIMED, claimed.result)
        # mimic what the form validation does
        claimed.clean()
        claimed.refresh_from_db()
        self.assertEqual(Test.RESULT_CLAIMED, claimed.result)
        claimed.result = Test.RESULT_PASS
        claimed.save(update_fields=["result"])
        claimed.refresh_from_db()
        options = [image for image in self.r_set.find_image(self.machine)]
        claims = Test.objects.filter(
            Q(claimer=prof, image=testing, language=self.language, profile=self.machine)
            & ~Q(result__in=[Test.RESULT_CLAIMED, Test.RESULT_PASS])
        )
        self.assertEqual(list(claims), [])

    def test_filter_offers(self):
        priorities = self.r_set.find_image(self.machine)
        for offer in priorities:
            self.assertEqual(offer.data["boot_method"], MachineProfile.BOOT_BIOS)
            self.assertIn(offer.architecture, ["amd64", "i386"])
        self.machine.boot_method = MachineProfile.BOOT_UEFI
        priorities = self.r_set.find_image(self.machine)
        for offer in priorities:
            self.assertEqual(offer.data["boot_method"], MachineProfile.BOOT_UEFI)
            self.assertIn(offer.architecture, ["amd64", "i386"])
