from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from organiser.models import Profile, Test, Architecture, MachineProfile
from organiser.utils import (
    InstallerImage,
    ReleaseSet,
    reverse_mode,
    check_item,
    NETWORK_ETHERNET,
    NETWORK_WIFI,
    NETWORK_DISABLED,
    NETWORK_FLAGS,
)


# This package is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License version 3 as
# published by the Free Software Foundation
# .
# This package is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
# .
# You should have received a copy of the GNU Affero General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


class TestDitto(TestCase):
    def test_settings(self):
        self.assertIsNotNone(settings.SOURCE_URL)
        self.assertIsNotNone(settings.BASE_RELEASE)


class TestUsers(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="first", email="second@third")
        self.user.profile.nick = "fourth"
        self.user.active = True
        self.user.save()

    def test_retrieve_nick(self):
        first = User.objects.get(username="first")
        self.assertIsNotNone(first)
        # test access via Profile to get status
        prof = Profile.objects.get(user=first)
        self.assertIsNotNone(prof)
        self.assertEqual(prof.nick, "fourth")
        self.assertEqual(first.profile.nick, "fourth")

    def test_valid_user(self):
        prof = Profile.objects.get(user=self.user)
        self.assertEqual(prof.status, Profile.STATUS_WAIT)
        self.assertTrue(prof.user.is_active)
        prof.status = Profile.STATUS_BLOCKED
        prof.save()
        prof.full_clean()
        self.assertFalse(prof.user.is_active)
        prof.status = Profile.STATUS_ACTIVE
        prof.save()
        prof.full_clean()
        self.assertTrue(prof.user.is_active)
        prof.status = Profile.STATUS_EMERITUS
        prof.save()
        prof.full_clean()
        self.assertFalse(prof.user.is_active)


class TestOrganiser(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="first", email="second@third")
        self.user.profile.nick = "fourth"
        self.user.save()
        Test.objects.all().delete()

    def test_release_set(self):
        release_set = ReleaseSet()
        self.assertIsNotNone(release_set)
        self.assertIsInstance(release_set.tests, list)
        self.assertIn("test", [list(detail.keys())[0] for detail in release_set.tests])
        details = [str(im) for im in release_set.images.values()]
        self.assertNotEqual([], details)


class TestInstallerImage(TestCase):
    def test_cd1(self):
        image = InstallerImage("debian-10.0.0-amd64-xfce-CD-1.iso", "001", None)
        self.assertEqual(image.version, "10.0.0")
        self.assertEqual(image.architecture, "amd64")
        self.assertEqual(image.type, "xfce-CD-1")
        self.assertIsNone(image.data)

    def test_underscore(self):
        self.assertRaises(
            ValueError, InstallerImage, "debian_10.0.0-amd64-xfce-CD-1.iso", "001", None
        )

    def test_multiarch(self):
        self.assertRaises(
            ValueError,
            InstallerImage,
            "debian-10.0.0-amd64-i386-netinst.iso",
            "004",
            None,
        )
        self.assertRaises(
            ValueError,
            InstallerImage,
            "debian-10.0.0-amd64-i386-netinst.iso",
            "004",
            None,
        )
        image = InstallerImage(
            "debian-10.0.0-amd64-i386-netinst.iso", "006", None, single_arch="i386"
        )
        self.assertEqual(image.version, "10.0.0")
        self.assertEqual(image.architecture, "i386")
        self.assertEqual(image.type, "netinst")
        image = InstallerImage(
            "debian-10.0.0-amd64-i386-netinst.iso", "004", None, single_arch="amd64"
        )
        self.assertEqual(image.version, "10.0.0")
        self.assertEqual(image.architecture, "amd64")
        self.assertEqual(image.type, "netinst")

    def test_constraints(self):
        image = InstallerImage("debian-10.0.0-amd64-BD.iso", "010", None)
        self.assertEqual(image.version, "10.0.0")
        self.assertEqual(image.architecture, "amd64")
        self.assertEqual(image.type, "BD")

    def test_methods(self):
        data = {
            "mode": reverse_mode("Graphical install"),
            "disk": "Simple disk: single fs",
            "filesystem": "EXT4",
            "desktop": "Mate",
        }
        image = InstallerImage("debian-10.0.0-amd64-BD.iso", "010", data)
        self.assertIsNotNone(image.data)
        self.assertEqual(image.data["mode"], Test.INSTALL_GRAPHICAL)


class TestMachineProfiles(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="first", email="second@third")
        self.user.profile.nick = "fourth"
        self.user.active = True
        self.user.save()
        Architecture.objects.get_or_create(name="amd64")
        self.release_set = ReleaseSet()

    def test_amd64_no_network_qemu(self):
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_VIRTUAL,
            network=NETWORK_DISABLED,
            boot_method=MachineProfile.BOOT_BIOS,
            name="tests_qemu_bios_amd64_1",
            command_line="qemu-system-x86 {{ imagefile }}",
            emulator="qemu",
            ram_size="2G",
            disc_size="10G",
            screen_keybd=True,
            owner=self.user,
        )
        options = [image.number for image in self.release_set.find_image(machine)]
        self.assertEqual(set({"003", "030", "130"}), set(options))

    def test_amd64_no_network_dell(self):
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_BARE_METAL,
            network=NETWORK_DISABLED,
            boot_method=MachineProfile.BOOT_UEFI,
            name="tests_dell_compact_uefi_amd64_1",
            model_name="Dell low profile desktop {{ imagefile }}",
            manufacturer="Dell",
            cpu_type="Intel",
            ram_size="4G",
            disc_size="980G",
            screen_keybd=True,
            owner=self.user,
        )
        numbers = [image.number for image in self.release_set.find_image(machine)]
        self.assertNotIn("050", numbers)
        self.assertEqual(set({"010", "010a", "021", "040", "110"}), set(numbers))

    def test_amd64_ethernet_only(self):
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_BARE_METAL,
            network=MachineProfile.NETWORK_IPV4_ONLY,
            ethernet=True,
            wifi=False,
            boot_method=MachineProfile.BOOT_UEFI,
            name="tests_dell_compact_uefi_amd64_1",
            model_name="Dell low profile desktop {{ imagefile }}",
            manufacturer="Dell",
            cpu_type="Intel",
            ram_size="4G",
            disc_size="980G",
            screen_keybd=True,
            owner=self.user,
        )
        ethernet_tests = [
            "000",
            "002",
            "010",
            "010a",
            "020",
            "021",
            "040",
            "110",
            "120",
            "203",
            "203a",
        ]
        numbers = [image.number for image in self.release_set.find_image(machine)]
        self.assertEqual(set(ethernet_tests), set(numbers))
        self.assertIn("000", numbers)
        self.assertIn(
            NETWORK_FLAGS[NETWORK_ETHERNET][1],
            self.release_set.lookup_number("000").describe(),
        )

    def test_amd64_wifi_available(self):
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_BARE_METAL,
            network=MachineProfile.NETWORK_IPV4_ONLY,
            wifi=True,
            boot_method=MachineProfile.BOOT_UEFI,
            name="tests_dell_compact_uefi_amd64_1",
            model_name="Dell low profile desktop {{ imagefile }}",
            manufacturer="Dell",
            cpu_type="Intel",
            ram_size="4G",
            disc_size="980G",
            screen_keybd=True,
            owner=self.user,
        )
        wifi_tests = [
            "010",
            "010a",
            "021",
            "040",
            "050",
            "050a",
            "060",
            "110",
            "002",
            "203",
            "203a",
            "120",
            "000",
            "020",
        ]

        numbers = [image.number for image in self.release_set.find_image(machine)]
        self.assertEqual(set(wifi_tests), set(numbers))
        self.assertIn("050", numbers)
        self.assertIn(
            NETWORK_FLAGS[NETWORK_WIFI][1],
            self.release_set.lookup_number("050").describe(),
        )

    def test_real_hardware(self):
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_BARE_METAL,
            network=MachineProfile.NETWORK_IPV4_ONLY,
            ethernet=True,
            boot_method=MachineProfile.BOOT_UEFI,
            name="tests_dell_compact_uefi_amd64_1",
            model_name="Dell low profile desktop {{ imagefile }}",
            manufacturer="Dell",
            cpu_type="Intel",
            ram_size="4G",
            disc_size="980G",
            screen_keybd=True,
            owner=self.user,
        )
        numbers = [image.number for image in self.release_set.find_image(machine)]
        real_hw = [
            "000",
            "002",
            "010",
            "010a",
            "020",
            "021",
            "040",
            "110",
            "120",
            "203",
            "203a",
        ]
        self.assertEqual(set(real_hw), set(numbers))

    def test_real_hardware_only(self):
        """ excludes tests which require real_hardware """
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_VIRTUAL,
            network=MachineProfile.NETWORK_IPV4_ONLY,
            ethernet=True,
            boot_method=MachineProfile.BOOT_BIOS,
            name="tests_qemu_bios_amd64_1",
            command_line="qemu-system-x86 {{ imagefile }}",
            emulator="qemu",
            ram_size="2G",
            disc_size="10G",
            screen_keybd=True,
            owner=self.user,
        )
        numbers = [image.number for image in self.release_set.find_image(machine)]
        real_hw = ["001", "003", "022", "030", "130", "202"]
        self.assertEqual(set(real_hw), set(numbers))

    def test_real_hardware_only_uefi(self):
        """ excludes tests which require real_hardware """
        arch = Architecture.objects.get(name="amd64")
        machine = MachineProfile.objects.create(
            architecture=arch,
            hardware=MachineProfile.HARDWARE_VIRTUAL,
            network=MachineProfile.NETWORK_IPV4_ONLY,
            ethernet=True,
            boot_method=MachineProfile.BOOT_UEFI,
            name="tests_qemu_bios_amd64_1",
            command_line="qemu-system-x86 {{ imagefile }}",
            emulator="qemu",
            ram_size="2G",
            disc_size="10G",
            screen_keybd=True,
            owner=self.user,
        )
        numbers = [image.number for image in self.release_set.find_image(machine)]
        real_hw = [
            "000",
            "002",
            "010a",
            "020",
            "021",
            "040",
            "110",
            "120",
            "203",
            "203a",
        ]
        self.assertEqual(set(real_hw), set(numbers))


class TestReleaseSet(TestCase):
    def setUp(self):
        self.release_set = ReleaseSet(testcase=True)

    def test_broken(self):
        self.assertTrue(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical install",
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                        "boot_method": "UEFI",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical install",
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "mode": "Graphical install",
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical install",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical install",
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical install",
                        "disk": "Simple disk: single fs",
                        "desktop": "Gnome",
                    }
                }
            )
        )
        self.assertFalse(
            check_item(
                {
                    "test": {
                        "number": "unittest",
                        "image": "debian-10.0.0-amd64-i386-netinst.iso",
                        "mode": "Graphical",  # not a valid reverse_mode() string
                        "disk": "Simple disk: single fs",
                        "filesystem": "XFS",
                        "desktop": "Gnome",
                    }
                }
            )
        )

    def test_lookup(self):
        self.assertIsNotNone(self.release_set.lookup_number(201))
        msg = self.release_set.lookup_number(201).describe()
        self.assertIsNotNone(msg)
        self.assertIsNotNone(self.release_set.lookup)
        self.assertIn(msg, self.release_set.lookup)
        image = self.release_set.lookup[msg]
        self.assertIsNotNone(image)
        self.assertEqual(image, "{0}".format(201))
        self.assertIsInstance(self.release_set.lookup_number(image), InstallerImage)
        installer = self.release_set.lookup_number(image)
        self.assertEqual(installer.number, "{0}".format(201))
        self.assertEqual(installer.describe(), msg)

    def test_release_version(self):
        self.assertIsNotNone(
            {installer.version for installer in self.release_set.images.values()}
        )
        self.assertIsNotNone(self.release_set.point_releases)

    def test_images(self):
        self.assertIsNotNone(self.release_set)
        self.assertIsNotNone(self.release_set.images)
        for _, value in self.release_set.images.items():
            self.assertNotEqual(
                value.data, {}, "Test number {0} is not valid".format(value.number)
            )
            self.assertNotEqual(
                value.data["mode"],
                Test.INSTALL_UNKNOWN,
                "Test number {0} is not valid".format(value.number),
            )
