#!/usr/bin/python3
#
# genfile.py
#
# Simple script to generate a new test yml file for the ditto project
# File can either be the previous release test yaml file or a template file
# The script will replace test serial number filed (number:) for each record
# with an incrementing serial number.  It will also update the version number
# to match that given on the command line.
#
# Initial version copyright 2020  Andy Simpkins (rattusrattus@debian.org)
# Version history
#   0.1   20200908 Andy Simpkins (rattusrattus@debian.org)
#         Initial version
#
# Use
#      genfile.py <input file> <version> <serial> <output file>
# where
#  input file - ditto compatible test file
#               this can be either a previous version, or from a template
#  version    - version number of the debian release to be tested
#               the script will match either a valid version number or the
#               string <VERSION>
#  serial     - initial serial number to begin numbering test cases from
#               the script will match either a valid quoted number or
#               "<SERIAL>"

import re
import sys
import os
import argparse


def genfile(inputfilename, version, serial, outputfilenname):
    print("Starting with the following arguments")

    print("Input file name is", end=": ")
    print(inputfilename)

    print("new version number is", end=": ")
    print(version)

    print("Starting from serial number", end=": ")
    print(serial)

    print("Output file name is", end=": ")
    print(outputfilenname)

    version = """-""" + version + """-"""

    # perform all recomples once (not each pass of a loop) because the output will always be the same
    serialsearch = re.compile(r"""\s*number:""")
    serialreplace = re.compile(r"""("<\w*>"|"\w*")""")
    versionsearch = re.compile(r"""\s*image:""")
    versionreplace = re.compile(r"""(-\w+\.\w+\.\w+-|-<\w*>-)""")

    fpo = open(outputfilenname, "w")
    with open(inputfilename, "r") as fpi:

        for line in fpi:
            # update number field
            # p = re.compile(r'''\s*number:''')
            m = serialsearch.match(line)
            if m:
                print("Updating (serial) number")
                # p = re.compile(r'''("<\w*>"|"\w*")''')
                s = '''"''' + str(serial) + '''"'''
                line = serialreplace.sub(s, line)
                serial = serial + 1

            # update version
            # format is
            # image: debian-<VERSION>-amd64-DVD-1.iso
            # image: firmware-<VERSION>-amd64-DVD-1.iso
            # p = re.compile(r'''\s*image:''')
            m = versionsearch.match(line)
            if m:
                print("Updating version (image) number")
                # p = re.compile(r'''(-\w+.\w+.\w+-|-<\w*>-)''')
                line = versionreplace.sub(version, line)

            # output (updated line) to outfile
            fpo.write(line)
            print(line, end="")

    print("Closing files...", end="")
    # fpi.close() This has already been closed automatically (with context)
    fpo.close()
    print("DONE")


def main():
    # replace this with libuary based code that parses arguments from any order i.e. -a x -b -c y
    # infile = sys.argv[1]
    # revision = sys.argv[2]
    # serialnum = int(sys.argv[3])
    # outfile = sys.argv[4]

    # use argparse to get the values for infile, revision, serialnum, outfile
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--infile", help="name of an input file", required=True)
    parser.add_argument(
        "-r",
        "--revision",
        help="revision number of the new debian release, for example buster second revision would be 10.2.0",
        required=True,
    )
    parser.add_argument(
        "-s",
        "--serialnum",
        type=int,
        help="first serial number to be used for this series of tests (this should be a higher number than has been previously used)",
        required=True,
    )
    parser.add_argument("-o", "--outfile", help="name of an output file", required=True)
    args = parser.parse_args()

    genfile(args.infile, args.revision, args.serialnum, args.outfile)
    # genfile(sys.argv[1], sys.argv[2], int(sys.argv[3]), sys.argv[4])


if __name__ == "__main__":
    main()
