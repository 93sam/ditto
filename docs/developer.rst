.. index:: developers

Developer notes
###############

Debian Images Team CI: |BadgeLink|_

.. |BadgeLink| image:: https://salsa.debian.org/images-team/ditto/badges/master/pipeline.svg
.. _BadgeLink: https://salsa.debian.org/images-team/ditto

https://docs.djangoproject.com/en/1.11/

https://getbootstrap.com/docs/4.0/getting-started/introduction/

http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#sections

https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone

https://wsvincent.com/django-user-authentication-tutorial-login-and-logout/

https://simpleisbetterthancomplex.com/tutorial/2018/08/13/how-to-use-bootstrap-4-forms-with-django.html

.. index:: software, dependencies

.. _required_software:

Required Software
*****************

* python3
* python3-django
* python3-pytest
* python3-pytest-django
* python3-pylint-django
* python3-sphinx
* python3-nose
* python3-yaml
* python3-sphinx-bootstrap-theme
* libjs-jquery
* libjs-bootstrap4
* gunicorn3

::

 sudo apt-get install python3 python3-django python3-pytest python3-pytest-django python3-pylint-django python3-sphinx python3-nose python3-yaml libjs-jquery python3-sphinx-bootstrap-theme gunicorn3 libjs-bootstrap4

.. index:: configuration

.. _configuration:

Configuration
*************

The organisation of the tests which can be claimed for any one release
can be a complex process. It is not realistic to run one test for every
individual option within Debian Installer. Tests have to be crafted into
a single set of combination tests which cover enough of the common use
cases that most users of Debian Installer will have most of their work
flow covered by at least one test.

This configuration set is encoded as a YAML file of numbered tests which
set a complete set of options.

.. note:: Ditto does not duplicate these tests and lacks a way to specify
   that any one test must be done on a virtual machine or on bare metal.
   See https://salsa.debian.org/images-team/ditto/issues/6

In addition to the YAML, Ditto needs to be configured to support
installer architectures and languages. You need to be a superuser
to do this, using the Django Administrative Interface.

.. index:: languages

.. _languages:

Languages
=========

Tests can be performed using different languages. Ditto needs to be
configured with a list of languages accepted by Debian Installer so
that users can choose the language to select within the test. Languages
can be added, removed and updated in the Adminstrator View and all
languages will be available for selection by any user.

.. index:: issues

.. _issues:

Issues
******

Ditto is a small project and has some acute restrictions on how tests,
profiles and workflow is defined. Ditto does not try particularly
hard to apply logical constraints to the test selection, leaving some
of the work to the maintainers creating the source YAML file and some
to the user in selecting a test.
