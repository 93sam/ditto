.. index:: claims

.. _claims:

Claiming a test
###############

To claim a test you need to:

#. Be an :ref:`active user <active_users>`.
#. Have at least one :ref:`machine profile <machine_profiles>`.

Once you are logged in, Ditto will show you a summary of any
outstanding claims (to be updated when you have completed or when you
abandon the test) and your virtual or bare metal machine profiles.

.. note:: It is recommended that you complete your outstanding test(s) 
   before starting a new one. This is to ensure that the test is 
   executed with all of the specified stages and options set.
   
   .. seealso:: :ref:`updating`.

Click on the ``Claim a test`` button for one of your available
machine profiles. From the list of descriptions, choose one test,
set the network options to match your test environment and set the
installer language you will be using.

.. _updating:

Updating a test
###############

Once a test has been claimed, it will show up in your outstanding
claims list. Complete the test and keep a note of any errors, warnings
or unexpected behaviour. (See Debian Installer wiki pages for help on
accessing DI log files.)

You can update the test by clicking on the ``Update this test`` button.

.. note:: You may find it useful to have a browser tab open on the
   update page whilst you are running the test to get a reminder of all 
   of the specified options.

Once the test is complete, set the result.

#. Pass if everything completes successfully.
#. Fail if something goes wrong
#. Abandon if you run out of time or are unable to complete the test
   for reasons unrelated to the test itself.

(Other options do nothing.)

If you choose Fail or Abandon, Ditto raises an Errata form. Please 
enter as much information as you can about the problem. Ditto does not 
currently support attaching log files - please discuss the details of 
the failure on the IRC channel.
