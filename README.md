The test organiser seeks to provide a friendly interface to selecting
and tracking the testing operations done by the Debian Images Team
during the preparation of the download images for a Debian release,
including point releases.

The team produce images suitable for CDs and DVDs to help people 
install Debian. That means that we need to work regularly with other 
teams to make things work:

* [debian-installer](https://wiki.debian.org/DebianInstaller)
* [the Release Team](https://wiki.debian.org/Teams/ReleaseTeam)
* [the architecture porters](https://wiki.debian.org/Ports)

Then the work splits into two parts:

Developing the debian-cd software itself Using that software regularly 
to generate and publish CD and DVD images, both daily/weekly for 
testing images and straight after each release for official stable 
images.
