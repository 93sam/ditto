import os
import yaml
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from organiser.models import Test, Profile, MachineProfile, vprint
from django.conf import settings

NETWORK_ETHERNET = 0
NETWORK_WIFI = 1
NETWORK_DISABLED = 2

NETWORK_FLAGS = (
    (NETWORK_ETHERNET, "Enabled"),
    (NETWORK_WIFI, "WiFi"),
    (NETWORK_DISABLED, "Disabled"),
)


def reverse_mode(mode_str):
    if mode_str == Test.INSTALL_CHOICES[0][1]:
        return Test.INSTALL_GRAPHICAL
    if mode_str == Test.INSTALL_CHOICES[1][1]:
        return Test.INSTALL_TEXT
    if mode_str == Test.INSTALL_CHOICES[2][1]:
        return Test.INSTALL_GRAPHICAL_EXPERT
    if mode_str == Test.INSTALL_CHOICES[3][1]:
        return Test.INSTALL_TEXT_EXPERT
    if mode_str == Test.INSTALL_CHOICES[4][1]:
        return Test.INSTALL_SPEECH
    return Test.INSTALL_UNKNOWN


def reverse_boot(boot_str):
    if boot_str == MachineProfile.BOOT_CHOICES[0][1]:
        return MachineProfile.BOOT_BIOS
    if boot_str == MachineProfile.BOOT_CHOICES[1][1]:
        return MachineProfile.BOOT_UEFI
    return None


def map_network_flags(flag_str):
    """
    The flags are discrete from MachineProfile.NETWORK_CHOICES
    and are not stored directly in the database. Simply used
    to map from YAML flags to the various database fields.

    This function is deceptive. The ethernet and wifi flags
    are internal to utils and not stored in the database.
    Instead, these flags are used to filter machine
    profiles using the booleans stored in the database.
    """
    if flag_str == NETWORK_FLAGS[0][1]:
        return NETWORK_FLAGS[0][0]
    if flag_str == NETWORK_FLAGS[1][1]:
        return NETWORK_FLAGS[1][0]
    return NETWORK_FLAGS[2][0]


def reverse_result(result_str):
    if result_str == Test.RESULT_CHOICES[1][1]:
        return Test.RESULT_CLAIMED
    if result_str == Test.RESULT_CHOICES[2][1]:
        return Test.RESULT_PASS
    if result_str == Test.RESULT_CHOICES[3][1]:
        return Test.RESULT_FAIL
    if result_str == Test.RESULT_CHOICES[4][1]:
        return Test.RESULT_ABANDONED
    return Test.RESULT_UNCLAIMED


def architecture_filter(offer, machine):
    if machine.architecture.name == "amd64":
        if offer.architecture == "amd64" or offer.architecture == "i386":
            return True
    if offer.architecture == machine.architecture.name:
        return True
    return False


def network_flag_filter(offer, machine):
    """
    WiFi can be used for tests which just need network enabled.
    WiFi can also be used for tests which require WiFi.
    Ethernet can only be use for tests which have network enabled.
    """
    if offer.data["network"] == NETWORK_WIFI and machine.wifi:
        return True
    if offer.data["network"] == NETWORK_ETHERNET and machine.ethernet:
        return True
    if offer.data["network"] == NETWORK_ETHERNET and machine.wifi:
        return True
    if offer.data["network"] == NETWORK_DISABLED:
        return True
    return False


def real_hardware_filter(offer, machine):
    if (
        offer.data["real_hardware"]
        and machine.hardware == MachineProfile.HARDWARE_BARE_METAL
    ):
        return True
    if not offer.data["real_hardware"]:
        return True
    return False


def valid_user_required(function):
    def wrapper(request, *args, **kwargs):
        if not request:
            return HttpResponseRedirect("/")
        user = User.objects.get(username=request.user)
        if not user.is_active:
            return HttpResponseRedirect("/")
        prof = Profile.objects.get(user=user)
        if prof.status != Profile.STATUS_ACTIVE:
            return HttpResponseRedirect("/")
        return function(request, *args, **kwargs)

    return wrapper


def check_item(item):
    if "number" not in item["test"]:
        vprint("A test is missing a test number - skipping")
        return False
    if "image" not in item["test"]:
        vprint(
            "Test number {0} is missing a test image - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    if "mode" not in item["test"]:
        vprint(
            "Test number {0} has no mode specified - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    if reverse_mode(item["test"]["mode"]) == Test.INSTALL_UNKNOWN:
        vprint(
            "Test number {0} has an unrecognised mode {1} - skipping".format(
                item["test"]["number"], item["test"]["mode"]
            )
        )
        return False
    if "boot_method" not in item["test"]:
        vprint(
            "Test number {0} has no boot_method specified - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    if reverse_boot(item["test"]["boot_method"]) > len(MachineProfile.BOOT_CHOICES):
        vprint(
            "Test number {0} has an unrecognised boot method {1} - skipping".format(
                item["test"]["number"], item["test"]["boot_method"]
            )
        )
        return False
    if "disk" not in item["test"]:
        vprint(
            "Test number {0} has no disk specified - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    if "filesystem" not in item["test"]:
        vprint(
            "Test number {0} has no filesystem specified - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    if "desktop" not in item["test"]:
        vprint(
            "Test number {0} has no desktop specified - skipping".format(
                item["test"]["number"]
            )
        )
        return False
    return True


class ReleaseSet:
    """
    Loads the YAML file for the configured release
    """

    def __init__(self, testcase=False):
        self.testcase = testcase
        # get the YAML file name from settings
        self.filename = "{0}.yaml".format(settings.BASE_RELEASE)
        yaml_path = os.path.join("share", self.filename)
        if not os.path.exists(yaml_path):
            raise ValueError("Misconfiguration: no such file {0}".format(yaml_path))
        with open(yaml_path, "r") as release:
            self.tests = yaml.safe_load(release)
        self.images = {}
        self.lookup = {}
        c = 0
        for item in self.tests:
            if not check_item(item):
                if testcase:
                    raise ValueError
                continue
            self.images[c] = InstallerImage(
                item["test"]["image"],
                item["test"]["number"],
                {
                    "mode": reverse_mode(item["test"]["mode"]),
                    "disk": item["test"]["disk"],
                    "filesystem": item["test"]["filesystem"],
                    "desktop": item["test"]["desktop"],
                    "boot_method": reverse_boot(item["test"]["boot_method"]),
                    "network": map_network_flags(item["test"]["network"]),
                    "real_hardware": item["test"]["real_hardware"],
                },
                single_arch=item["test"].get("architecture", None),
            )
            self.lookup[self.images[c].describe()] = self.images[c].number
            c += 1
        self.point_releases = {installer.version for installer in self.images.values()}

    def lookup_number(self, number):
        number = "{0}".format(number)
        for _, value in self.images.items():
            if value.number == number:
                return value
        return None

    def find_image(self, machine):
        # Needs to check critical options are available with this machine.
        # network: WiFi|Enabled|Disabled
        # WiFi needs user to then configure DI to connect to the WiFi.
        # assumes a valid WiFi infrastructure.
        # DI will offer the list of SSIDs and then handle passphrases.
        options = []
        numbers = [
            item.number
            for item in Test.objects.filter(
                result__in=[Test.RESULT_CLAIMED, Test.RESULT_PASS]
            )
        ]
        for _, value in self.images.items():
            if not architecture_filter(value, machine):
                continue
            if value.number in numbers:
                continue
            if value.data["boot_method"] != machine.boot_method:
                continue
            if not network_flag_filter(value, machine):
                continue
            if not real_hardware_filter(value, machine):
                continue
            vprint(value.describe())
            options.append(value)
        return options


class InstallerImage:
    """
    Load a list of installer image names based on
    filename rules.
    debian-10.0.0-amd64-netinst.iso
    "{0}-{1}-{2}-{3}.iso".format(debian, version, architecture, type)
    Populates data structure based on the specified test data.
    """

    def __init__(self, name, number, data, single_arch=None):
        self.name = name
        self.number = "{0}".format(number)
        self.data = data
        if "-" not in name:
            raise ValueError("Invalid Image name")
        if not name.startswith("debian") and not name.startswith("firmware"):
            raise ValueError("Not a debian image")
        if name.count("-") < 3:
            raise ValueError("Not a correctly formatted image name")
        # drop .iso & split at the first three hyphens only.
        (chk, vers, chunk) = name[:-4].split("-", 2)
        if chunk.startswith("amd64-i386"):
            # other multiarch would need a similar check
            (_, i_type) = chunk.rsplit("-", 1)
            arch = single_arch
            if not arch:
                raise ValueError("Invalid multiarch parameters")
        else:
            (arch, i_type) = chunk.split("-", 1)
        if chk not in ("debian", "firmware"):
            raise ValueError("Incorrect image name")
        self.version = vers
        self.architecture = arch
        self.type = i_type

    def populate_test(self, test):
        test.number = self.number
        test.mode = self.data["mode"]
        test.disk = self.data["disk"]
        test.filesystem = self.data["filesystem"]
        test.desktop = self.data["desktop"]
        test.point = self.version
        test.save()

    def describe(self):
        data = [
            "Install mode: {0}".format(Test.INSTALL_CHOICES[self.data["mode"]][1]),
            "Disk setup: {0}".format(self.data["disk"]),
            "Desktop: {0}".format(self.data["desktop"]),
            "Filesystem: {0}".format(self.data["filesystem"]),
            "Boot method: {0}".format(
                MachineProfile.BOOT_CHOICES[self.data["boot_method"]][1]
            ),
            "Network config: {0}".format(NETWORK_FLAGS[self.data["network"]][1]),
        ]
        return "{0} {1} {2}".format(self.number, self, " ".join(data))

    def __str__(self):
        return "{0} {1} {2}".format(self.version, self.architecture, self.type)
